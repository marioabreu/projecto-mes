module.exports = {

  index: function(req, res) {
    res.render('index');
  },

  about: function(req, res) {
    res.render('about');
  },

  contact: function(req, res){
  	res.render('contact');
  },

  requests: function(req, res){
    res.render('requests');
  },

  routes: function(req, res){
    res.render('routes');
  },

  form: function(req, res){
    res.render('/clients/form');
  },
  
  statistics: function(req, res){
    res.render('statistics');
  }
}
