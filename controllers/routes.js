module.exports = function(router){
  router.get('/', function(req, res) {
    res.render('routes', {title: 'Routes'});
  });

  return router;
};

