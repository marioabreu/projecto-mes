var Client = require('../models/client')();

module.exports = function(){

  var index = function(req, res){
    
    Client.all(function(err, data){
      if(req.params.format === '.json')
        res.json(data);
      else
        res.render('clients', {items: data});
    });
  
  };

  var destroy = function(req, res, next){
    console.log('controller destroy');
    Client.delete(req.params.id, function(err, data){
      res.render('clients', {items: data});
    });
    
  };
  
  var create = function(req, res, next){
    // console.log(Client);
    Client.create(req.body, function(error, data){
      res.render('clients', {items: data});   
    });

   
    
  };

  var show = function(req, res){
    // show client
    console.log('show client');
    Client.show(req.params.id, function(client){
      console.log(client);
      res.json(client);
    });
    
  };



  var update = function(req, res, next){
    
  };

  var requests = function(req, res){
    console.log('requests');
    Client.requests(req.params.id, function(error, data){
      console.log(data);
      res.render('requests');
    });
  };

 return {
    index: index,
    show: show,
    create: create,
    update: update,
    delete: destroy,
    requests: requests
 }

}

 
  

/* GET users listing. */
// router.get('/', function(req, res) {
//   res.render('clients', {title: 'Client\'s List'});
// });

// router.get('/clientslist', function(req, res){
//   var db = req.db;
//   db.collection('clientlist').find().toArray(function(err, items){
//     res.json(items);
//   });
// });

// router.post('/addClient', function(req, res){
//   var db = req.db;
//   db.collection('clientlist').insert(req.body, function(err, result){
//     res.send((err === null) ? {msg:''} : {msg:'Erro' + err});
//   });
// });

//  db.clientlist.update({name: 'Mário'}, {$push: {requests: {cid: 2, date:04/05/2014, need:10000}}})
// router.post('/newrequest', function(req, res){
//   var db = req.db;
//   db.collection('clientlist').updateById( req.body.clientId, {$push: {requests: req.body.requests}}, function(err, result){
//     res.send((err === null) ? {msg:''} : {msg:'Erro' + err});
//   });
// });

// router.delete('/deleteclient/:id', function(req, res){
//   var db = req.db;
//   var clientToDelete = req.params.id;
//   db.collection('clientlist').removeById(clientToDelete, function(err, result){
//     res.send((result===1) ? {msg: ''} : {msg:'Erro: ' + err});
//   });
// });


// router.post('/editclient/:id', function(req, res){
//   var clientToEdit = req.params.id;

//   db.collection('clientlist').updateById(clientToEdit, {$set: req.body });
// });


// router.get('/requests', function (req, res){
//   var db = req.db;
//   db.collection('clientlist').find().toArray(function(err, items){
//     res.json(items.map(function(arrayItem){ return arrayItem.requests}));
//   });
// });


// router.post('/deleterequest', function(req, res){
//   var db = req.db;
//   console.log(req.body.request);
//   console.log(req.body.clientId);

//   console.log('db.clientlist.update({_id:'+ req.body.clientId +'}, {$pull: {requests: { cid : '+req.body.request+' }}})');
//   db.collection('clientlist').update({_id: req.body.clientId} , {$pull: {requests: {cid : req.body.requestId }}}, function(err, result){
//     res.send((err === null) ? {msg:''} : {msg:'Erro' + err});
//   });
// });

