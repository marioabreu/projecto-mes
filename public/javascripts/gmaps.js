document.addEventListener('DOMContentLoaded', drawMap);
document.addEventListener('DOMContentLoaded', drawClients);
document.addEventListener('DOMContentLoaded', getClients);




$(document).ready(function(){

  $('#getDistances').on('click', getClients);
  $('#getDistancesOnly1').on('click', calculateDistances1);
  $('#getDistancesOnly2').on('click', calculateDistances2);
  $('#getDistancesOnly3').on('click', calculateDistances3);
  $('#getDistancesOnly4').on('click', calculateDistances4);
  $('#getChance').on('click', chanceMarkers);
  $('#saveClientes').on('click', teste);
  $('#saveClientesOther').on('click', testeOther);
  $('#clearInstance').on('click', clear);
  $('#plus').on('click', plus);
  $('#minus').on('click', minus);
  $('#getClientInfo').on('click', getClientInfo);
  $('#getCoordinates').on('click', getCoordinates);

});

window.onload = function(){
  showMarkers;

}

var distances = [];
var map;
var mapClients;
var mapCenter = new google.maps.LatLng(41.1208576,-8.6146304);
var marker1;
var markers = new Array();
var clients = [];
var clientsLatLng = [];
var origens = [];
var array1 = new  Array();
var array2 = new  Array();
var array3 = new  Array();
var array4 = new  Array();
var coordinatesTeste = [];
var generated = [];
var diamond =0;
var toGenerate= 25;
var markersGenerated = new Array();
var armazem = 0;
//

function plus(){
  diamond+=1;
  console.log(diamond);
  document.getElementById('diamond').innerText = 'Indice: ' + diamond;
}

function minus(){
  diamond-=1;
  console.log(diamond);
  document.getElementById('diamond').innerText = 'Indice: ' + diamond;
}

function getClients(){
  var lat, lng;
    clients = [];
    clientsLatLng = [];
  $.getJSON( '/clients.json', function( data ) {
    clients = data;

    for (var i=0; i<clients.length; i++){
      var markerAdded = addMarker(clients[i]);
      origens.push(
        new google.maps.LatLng(
          clients[i].location.split(',')[0],
          clients[i].location.split(',')[1]
        ));

      if(i>=75)
        array4.push(
          new google.maps.LatLng(
            clients[i].location.split(',')[0],
            clients[i].location.split(',')[1]
        ));
      else
        if(i>=50)
          array3.push(
            new google.maps.LatLng(
              clients[i].location.split(',')[0],
              clients[i].location.split(',')[1]
          ));
        else
          if(i>=25)
            array2.push(
              new google.maps.LatLng(
                clients[i].location.split(',')[0],
                clients[i].location.split(',')[1]
            ));
          else
            array1.push(
              new google.maps.LatLng(
                clients[i].location.split(',')[0],
                clients[i].location.split(',')[1]
            ));


      clientsLatLng.push(
        new google.maps.LatLng(
            clients[i].location.split(',')[0],
            clients[i].location.split(',')[1]
        ));
    }
  });
};

function clear(){
  document.getElementById("instanceResult").value = "";
}

function teste(){

  document.getElementById("instanceResult").value = "";
  c = [];

  markers.forEach( function(a) { c.push(a.getPosition().lat()+','+a.getPosition().lng())} );
  
  console.log(c);
  c.reverse();

  var id = 1;
  clients.forEach( function(a) {
    a.id = id;
    console.log(c);
    a.location = c.pop();
    a.name = chance.name();
    a.company = chance.last()
    a.phone = chance.phone();
    a.email = chance.email({domain:'gmail.com'});
    a.need = chance.integer({min:2000, max:10000});
    a.startWindow = chance.integer({min:0, max:6}) * 9;
    a.endWindow = 90 - (chance.integer({min:0, max:3}) * 9) ;
    a.priority = chance.integer({min:1, max:3});
    a.nWines = chance.integer({min: 2, max: 3});
    id++;
  })

  // Nodo 1 e nodo N têm a mesma localização
  clients[clients.length-1].location = markers[0].getPosition().lat()+','+markers[0].getPosition().lng();

  // Garantir que o nodo 1 e nodo N têm a janela temporal total!
  clients[0].startWindow = 0;
  clients[0].endWindow = 90;
  clients[clients.length-1].startWindow = 0;
  clients[clients.length-1].endWindow = 90;

  // Não existem prémios no nodo 1 nem no nodo N
  clients[0].need = 0;
  clients[clients.length-1].need =  0;

  // Tipos de vinhos a zero nos nodos 1 e N
  clients[0].nWines = 0;
  clients[clients.length-1].nWines =  0;


  a = "db.dropDatabase();\nvar clients = " + JSON.stringify(clients, null, ' ') + ";\ndb.clients.save(clients);";
  console.log(a);
  document.getElementById("instanceResult").value = a ;

}

// A função teste gera clientes para horizontes de planeamento de 90 horas, ou seja de 10 dias úties.
// na função testeOther, estamos a criar uma réplica da função para a utilização com um horizonte de planeamento diferente. 
// neste caso vamos utilizar um horizonte de planeamento de 4 semanas.

function testeOther(){

  document.getElementById("instanceResult").value = "";
  c = [];

  markers.forEach( function(a) { c.push(a.getPosition().lat()+','+a.getPosition().lng()) } );

  c.reverse();
  var id = 1;

  clients.forEach( function(a) {
    a.id = id;
    a.location = c.pop();
    a.name = chance.name();
    a.company = chance.last()
    a.phone = chance.phone();
    a.email = chance.email({domain:'gmail.com'});
    a.need = chance.integer({min:2000, max:10000});
    a.startWindow = chance.integer({min:0, max:12}) * 9;
    a.endWindow = 180 - (chance.integer({min:0, max:6}) * 9) ;
    a.priority = chance.integer({min:1, max:3});
    a.nWines = chance.integer({min: 2, max: 3});
    id++;
  })

  // Nodo 1 e nodo N têm a mesma localização
  clients[clients.length-1].location = markers[0].getPosition().lat()+','+markers[0].getPosition().lng();

  // Garantir que o nodo 1 e nodo N têm a janela temporal total!
  clients[0].startWindow = 0;
  clients[0].endWindow = 180;
  clients[clients.length-1].startWindow = 0;
  clients[clients.length-1].endWindow = 180;

  // Não existem prémios no nodo 1 nem no nodo N
  clients[0].need = 0;
  clients[clients.length-1].need =  0;

  // Tipos de vinhos a zero nos nodos 1 e N
  clients[0].nWines = 0;
  clients[clients.length-1].nWines =  0;

  a = "db.dropDatabase();\nvar clients = " + JSON.stringify(clients, null, ' ') + ";\ndb.clients.save(clients);";
  console.log(a);
  document.getElementById("instanceResult").value = a ;

}

function drawMap() {

  var makeMap = document.getElementById('my-map');

  if (makeMap !== null) {
    console.log('drawMap');

    var mapOptions = {
      center: new google.maps.LatLng(41.1208576,-8.6146304),
      zoom: 6,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      draggable: true,
      zoomControl: true
    };

    map = new google.maps.Map(makeMap, mapOptions);

    marker1 = new google.maps.Marker({
      'position': mapCenter,
      'map': map,
      'draggable': true
    });
  }
};

function chanceMarkers(){
  var location;

  deleteMarkers();
  console.log('PASSEI!');

  for (var i = 0; i<clients.length-1 ; i++){
    lat = chance.latitude({min: 37.234677, max:41.668785});
    lng = chance.longitude({min: -8.721740, max:-7.469298});
    // console.log(lat.toString().substr(0,10)+','+lng.toString().substr(0,10));
    location = new google.maps.LatLng(lat, lng);

    var marker = new google.maps.Marker({
      'position' : location,
      'map' : mapClients
    });

    markers.push(marker);
  }
};

function drawClients(){

  var makeMap = document.getElementById('clientsOnMap');
  if (makeMap !== null){
    console.log('drawClients');

    var mapOptions = {
      'center' : new google.maps.LatLng(40.001825,-8.2934144),
      'zoom' : 6,
      'mapTypeId' : google.maps.MapTypeId.ROADMAP,
      'draggable' : true,
      'zoomControl' : true
    };

    mapClients = new google.maps.Map(makeMap, mapOptions);

    showMarkers('clientsOnMap');
  }
};

function setAllMap(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

function clearMarkers(){
  setAllMap(null);
}


function showMarkers(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(mapClients);
  }
};

function deleteMarkers() {
  clearMarkers();
  markers = [];
}

function generate(){

    // guimarães
    // 41.535440, -8.383802
    // 41.394970, -8.223814
    console.log('cleaning!');
    while(generated.length){
        generated.pop();
        markers.pop()
    }

    sleep(500);
    console.log('Random data generated!');
    for (var i = 0; i<toGenerate; i++){
      lat = chance.latitude({min: 37.234677, max:41.668785});
      lng = chance.longitude({min: -8.721740, max:-7.469298});
      // console.log(lat.toString().substr(0,10)+','+lng.toString().substr(0,10));
      generated.push(lat.toString()+','+lng.toString());
      console.log(generated[i])
    }

    console.log('------------------------------');
}


function addMarker(client) {

  // ## Este codigo é para utilizado para guardar a latitude e longitude dos clientes da Base de dados.
  // ## Para gerar dados, estamos a utilizar chance.js
  var lat = parseFloat(client.location.split(',')[0]);
  var lng = parseFloat(client.location.split(',')[1]);

  // var lat = chance.latitude({min: 37.234677, max:41.668785});
  // var lng = chance.longitude({min: -8.721740, max:-7.469298});

  var location = new google.maps.LatLng(lat, lng);

  var contentString = "<h2>"+ client.name +"</h2><p> "+ client.company +" </p>";

  var infoWindow = new google.maps.InfoWindow(function() {
    content : contentString;
  });

  var marker = new google.maps.Marker({
    'position' : location,
    'map' : mapClients
  });

  google.maps.event.addListener(marker, 'click', function() {
    infoWindow.open(mapClients, marker);
  })

  markers.push(marker);

  return marker;
};


function calculateDistances1() {

  var service = new google.maps.DistanceMatrixService();
    console.log(service);
    service.getDistanceMatrix({
      origins: [origens[diamond]],
      destinations: array1,
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
    }, callback);

};

function calculateDistances2() {


  var service = new google.maps.DistanceMatrixService();

      service.getDistanceMatrix({
        origins: [origens[diamond]],
        destinations: array2,
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
      }, callback);
};

function calculateDistances3() {


  var service = new google.maps.DistanceMatrixService();
      service.getDistanceMatrix({
        origins: [origens[diamond]],
        destinations: array3,
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
      }, callback);

};

function calculateDistances4() {


  var service = new google.maps.DistanceMatrixService();


      service.getDistanceMatrix({
        origins: [origens[diamond]],
        destinations: array4,
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
      }, callback);

};


function getClientInfo(){
  var aux='';
  clients.forEach(function(a){
    aux += a.need+'\t'+a.startWindow+'\t'+ a.endWindow+'\t'+a.priority+'\t'+a.nWines+'\n';
  });

  // for(var i = 0; i<aux.length; i++){
    document.getElementById('cenas').value += aux;
    // document.getElementById('cenas').value += '\n';
  // }
}


function getCoordinates(){
  var aux='';
  clients.forEach(function(a){
    aux +=a.location +'\n';
  });

  // for(var i = 0; i<aux.length; i++){
    document.getElementById('cenas').value += aux;
    // document.getElementById('cenas').value += '\n';
  // }
}



function callback(response, status) {

  if (status != google.maps.DistanceMatrixStatus.OK) {
    alert('Error was: ' + status);
  } else {

    var origins = response.originAddresses;
    var destinations = response.destinationAddresses;
    var aux = '';

      for (var a = 0; a < origins.length; a++) {
        var results = response.rows[a].elements;
        for (var j = 0; j < results.length; j++) {
          aux += (Math.round(results[j].distance.value/1000)) + '\t';
          document.getElementById('cenas').value += (Math.round(results[j].distance.value/1000) / 40) + '\t';
          }
      }
      console.log(aux);
      aux = '';
      document.getElementById('cenas').value += '\n';
  }
};

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if((new Date().getTime()-start) > milliseconds){
      break;
    }
  }
};





