// Clientlist data array for filling in info box
var clientListData = [];
var requests = [];

// DOM Ready =============================================================

$(document).ready(function() {

    // Populate the client table on initial page load
    populateTable();
    $('#clientList table tbody').on('click', 'td a.linkshowclient', showClientInfo);
    $('#btnAddClient').on('click', addClient);
    $('#clientList table tbody').on('click', 'td a.linkdeleteclient', deleteClient);
    $('#clientList table tbody').on('click', 'td a.clientrequets', showClientRequests);
    $('#clientList table tbody').on('click', 'td a.newRequest', newRequest);
    $('#requests table tbody').on('click', 'td a.linkdeleterequest', deleteRequest);
    $('#btnNewRequest').on('click', makeNewRequest);
    $('#clientList table tbody').on('click', 'td a.editClient', editClient);


});

// Functions =============================================================

// Fill table with data
// SHOW CLIENTS AND POPULATE CLIENT LIST

function populateTable() {

    // Empty content string
    var tableContent = '';
    // jQuery AJAX call for JSON
    $.getJSON( '/clients.json', function( data ) {
        clientListData = data;
        // For each item in our JSON, add a table row and cells to the content string
        $.each(data, function(){
            tableContent += '<tr>';
            tableContent += '<td><a href="clients/'+ this._id+'" class="linkshowclient" rel="' + this.name + '" title="Show Details">' + this.name + '</td>';
            tableContent += '<td>' + this.email + '</td>';
            tableContent += '<td><a href="#" class="linkdeleteclient" rel="' + this._id + '">delete</a></td>';
            tableContent += '<td><a href="/clients/'+this._id+'/requests" class="clientrequets" rel="' + this._id + '">requests</a></td>';
            tableContent += '<td><a href="#" class="newRequest" rel="' + this._id + '">Make new request</a></td>';
            tableContent += '<td><a href="#" class="editClient" rel="' + this._id + '">Edit Client</a></td>';
            tableContent += '</tr>';
        });

        // Inject the whole content string into our existing HTML table
        $('#clientList table tbody').html(tableContent);
    });
};


// Show Client Info HOMEPAGE
function showClientInfo(event) {
    event.preventDefault();

    // guardar o nome do cliente através do link
    var thisClientName = $(this).attr('rel');

    var arrayPosition = clientListData.map(function(arrayItem) { return arrayItem.name;}).indexOf(thisClientName);

    // Get our User Object
    var thisClientObject = clientListData[arrayPosition];

    //Populate Info Box
    $('#clientInfoAutoId').text(thisClientObject._id);
    $('#clientInfoId').text(thisClientObject.id);
    $('#clientInfoName').text(thisClientObject.name);
    $('#clientInfoCompany').text(thisClientObject.company);
    $('#clientInfoEmail').text(thisClientObject.email);
    $('#clientInfoPhone').text(thisClientObject.phone);
    $('#clientInfoLocation').text(thisClientObject.location);
    $('#clientInfoRequestsLength').text(thisClientObject.requests[0]);
    $('#clientInfoCreatedAt').text(thisClientObject.created_at);
    $('#clientInfoUpdatedAt').text(thisClientObject.updated_at);
    $('#clientInfoDeleted').text(thisClientObject.deleted);


};

function showClientRequests(event) {
    event.preventDefault();

    var tableContent = '';
    // guardar o nome do cliente através do link
    var thisClientId = $(this).attr('rel');

    var arrayPosition = clientListData.map(function(arrayItem) { return arrayItem._id;}).indexOf(thisClientId);

    // Get our User Object
    var thisClientObject = clientListData[arrayPosition];

    // console.log('thisClientObject[\'requests\'].length:' + thisClientObject['requests'].length);

    for (var i=0; i< thisClientObject['requests'].length; i++ ){
        tableContent += '<tr>';
        tableContent += '<td> ' + thisClientObject['requests'][i].request_id + '</td>';
        tableContent += '<td> ' + thisClientObject['requests'][i].need + '</td>';
        tableContent += '<td> ' + thisClientObject['requests'][i].earlyWindow + '</td>';
        tableContent += '<td> ' + thisClientObject['requests'][i].lateWindow + '</td>';
        tableContent += '<td> ' + thisClientObject['requests'][i].status + '</td>';
        tableContent += '<td><a href="#" class="linkdeleterequest" rel="' + thisClientObject['requests'][i].cid + '">delete</a></td>';
        tableContent += '</tr>\n';
    };

    $('#requests table tbody').html(tableContent);
    document.getElementById('owner').innerHTML = thisClientObject.name;
};

function makeNewRequest(event){

    event.preventDefault();

    var thisClientName = document.getElementById('requestName').innerHTML;
    var arrayPosition = clientListData.map(function(arrayItem) { return arrayItem.name;}).indexOf(thisClientName);
    var thisClientObject = clientListData[arrayPosition];
    var errorCount=0;
    var today =  Date.now();



    if( $('#inputRequestDate').val() === '' || $('#inputRequestNeed').val() === ''){
        console.log('u left some fields behind!');
        errorCount = 1;
    }

    // Check and make sure errorCount's still at zero
    if(errorCount === 0) {
        // If it is, compile all user info into one object
        var updateClient = {
            'requests' : {
                request_id: today,
                date: document.getElementById('inputRequestDate').value,
                need: document.getElementById('inputRequestNeed').value,
                status: 'undone'
            },
            'clientId' : thisClientObject._id
        };

        // add request to a global array to use later for remove/update de request.
        requests.push({request_id: today,
            date: document.getElementById('inputRequestDate').value,
            need: document.getElementById('inputRequestNeed').value,
            status: 'undone'
        });

        console.log(requests[0]);
        // Use AJAX to post the object to our adduser service
        $.ajax({
            type: 'POST',
            data: updateClient,
            url: '/clients/newrequest',
            dataType: 'JSON'
        }).done(function( response ) {
            // Check for successful (blank) response
            if (response.msg === '') {
                // Clear the form inputs
                $('input').val('');
                // Update the table
            }
            else {

                // If something goes wrong, alert the error message that our service returned
                alert('Error: ' + response.msg);
                populateTable();

            }
        });
    }
    else {
        // If errorCount is more than 0, error out
        alert('u left some fields behind!');
        return false;
    }

    console.log('existing requests: '+ requests.length);
};

function todaysDate(){

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getYear() + 1900;

    if(dd<10) {
        dd='0'+dd
    }
    if(mm<10) {
        mm='0'+mm
    }

    return dd + '/' + mm + '/' + yyyy;
};

function addClient(event){

    event.preventDefault();
    // Super basic validation - increase errorCount variable if any fields are blank
    var errorCount = 0;
    var clientLocation = new google.maps.LatLng(marker1.getPosition().k, marker1.getPosition().A);

    // Now that we have clients location saved on clientLocation variable, lets user service distanceMatrix from google.


    $('#addClient input').each(function(index, val) {
        if($(this).val() === '') { errorCount++; }
    });

      // Check and make sure errorCount's still at zero

    if(errorCount === 0) {

        // If it is, compile all user info into one object

        var now = Date();
        var newClient = {
            'name': $('input#inputClientName').val(),
            'email': $('input#inputClientEmail').val(),
            'phoneNumber': $('input#inputClientPhone').val(),
            'location' : marker1.position.toString().slice(1,marker1.getPosition().toString().length-1),
            'requests' : [0],
            'created_at' : now,
            'updated_at' : now,
            'deleted' : false
        }

        // Use AJAX to post the object to our adduser service
        $.ajax({
            type: 'POST',
            data: newClient,
            url: '/clients',
            dataType: 'JSON'
        }).done(function( response ) {
            // Check for successful (blank) response
            if (response.msg === '') {
                // Clear the form inputs
                $('input').val('');

                // Update the table
                populateTable();

            }
            else {

                // If something goes wrong, alert the error message that our service returned
                alert('Error: ' + response.msg);

            }
        });

    }
    else {
        // If errorCount is more than 0, error out
        alert('Please fill in all fields');
        return false;
    }
};



function dealWithResponse(response, status) {

  if (status == google.maps.DistanceMatrixStatus.OK) {
    var origins = response.originAddresses;
    var destinations = response.destinationAddresses;

    var res = new Array();
    for (var i = 0; i < origins.length; i++) {
      var results = response.rows[i].elements;
      for (var j = 0; j < results.length; j++) {
        var element = results[j];
        var distance = element.distance.text;
        var duration = element.duration.text;
        var from = origins[i];
        var to = destinations[j];
        res.push(element.distance.text);
      }
    }
}


    // if (status != google.maps.DistanceMatrixStatus.OK) {
    //     alert('Error was: ' + status);
    // } else {
    //     var origins = response.originAddresses;
    //     var destinations = response.destinationAddresses;
    //     var outputDiv = document.getElementById('outputDiv');
    //     var counter = 0;

    //     console.log(response.rows[0]);
    //     for(var i=0; i<response.rows[0].length; i++){
    //         console.log(response.rows[0].elements[i].distance.text);
    //     }

        // // don't save show distances between equal points!
        // for (var i = 0; i < origins.length; i++) {
        //     var results = response.rows[i].elements;
        //     for (var j = 0; j < results.length; j++) {
        //         if (i!=j){
        //             outputDiv.innerHTML += origins[i] + ' to ' + destinations[j]
        //             + ': ' + results[j].distance.text + ' in '
        //             + results[j].duration.text + '<br>';
        //             counter++;
        //         }
        //     }
        // }
    // }
};


function editClient(event){
    event.preventDefault();

    var thisClientName = $(this).attr('rel');
    console.log($(this).attr('rel'));
    var arrayPosition = clientListData.map(function(arrayItem) { return arrayItem.name;}).indexOf(thisClientName);

    // Get our User Object
    var thisClientObject = clientListData[arrayPosition];

    var errorCount = 0;

    $('#addClient input').each(function(index, val) {
        if($(this).val() === '') { errorCount++; }
    });


      // Check and make sure errorCount's still at zero
    if(errorCount === 0) {
                // If it is, compile all user info into one object

        var updateClient = {
            'name': $('input#inputClientName').val(),
            'email': $('input#inputClientEmail').val(),
            'phoneNumber': $('input#inputClientPhone').val(),
            'location' : marker1.position.toString().slice(1,markar1.getPosition().toString().length-1),
            'requests' : new Array()
        }

        // Use AJAX to post the object to our adduser service
        $.ajax({
            type: 'PUT',
            data: updateClient,
            url: '/clients/' + $(this).attr('rel'),
            dataType: 'JSON'
        }).done(function( response ) {
            // Check for successful (blank) response
            if (response.msg === '') {
                // Clear the form inputs
                $('input').val('');

                // Update the table
                populateTable();

            }
            else {

                // If something goes wrong, alert the error message that our service returned
                alert('Error: ' + response.msg);

            }
        });

    }
    else {
        // If errorCount is more than 0, error out
        alert('Please fill in all fields');
        return false;
    }
};

function deleteClient(event){
    event.preventDefault();

    // var confirmation = confirm('Are you sure you want to delete this user?');
    var confirmation = confirm('Are you sure you want to delete this user?');

    var arrayPosition = clientListData.map(function(arrayItem) { return arrayItem._id;}).indexOf($(this).attr('rel'));
    
    console.log('arrayPosition: ' + arrayPosition);
    console.log('object: ' + clientListData[arrayPosition]._id);

    if(confirmation){
        $.ajax({
            type: 'DELETE',
            url: '/clients/' + clientListData[arrayPosition]._id
        }).done(function (response){
            if(response.msg !== ''){
                alert('Error: ' + response.msg);
            }
            //update the table
            populateTable();
        });
    } else{

        // If they said no to the confirm, do nothing
        return false;
    }
};

function print(o){
    var resultStr = '';

    for(var p in o){
        resultStr += p + ': ' +o[p][0]+'\n';
    }

    return resultStr;
};

// Ainda não funciona
function deleteRequest(event){
    event.preventDefault();

    var thisClientName = document.getElementById('owner').innerHTML;
    var arrayPosition = clientListData.map(function(arrayItem) { return arrayItem.name;}).indexOf(thisClientName);
    var thisClientObject = clientListData[arrayPosition];

    var arrayPositionRequest = requests.map(function(arrayItem) { return arrayItem.request_id;}).indexOf($(this).attr('rel'));
    console.log(arrayPositionRequest);
    var confirmation = confirm('Are you sure you want to delete this request?');


    var updateClient = {
        'clientId' : 'ObjectId('+'\"'+thisClientObject._id+'\")',
        'request' : requests[arrayPositionRequest]
    }

    if(confirmation){
        $.ajax({
            type: 'POST',
            data: updateClient,
            url: 'clients/deleterequest'
        }).done(function(response){
            if (response.msg != ''){
                alert('Error: ' + response.msg);
            }
        });

    } else{
        return false;
    }
};

function newRequest(event){
    event.preventDefault();

    var thisClientId = $(this).attr('rel');
    var arrayPosition = clientListData.map(function(arrayItem) { return arrayItem._id;}).indexOf(thisClientId);
    var thisClientObject = clientListData[arrayPosition];

    document.getElementById('requestName').innerHTML = thisClientObject.name;
    document.getElementById("inputRequestDate").focus();
};

