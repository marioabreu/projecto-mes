db.dropDatabase();

var clients = [
 {
  "id": 2,
  "name": "João Rodrigues da Silva",
  "company": "CARM",
  "email": "jrsilva@aquasseur.com",
  "phone": "+351 279718010",
  "location": "41.18496,-7.444680000000062"
 },
 {
  "id": 3,
  "name": "Joana Santos Ferreirinha",
  "company": "Casa Ferreirinha",
  "email": "jferreirinha@ferreirinha.pt",
  "phone": "+351 227838104",
  "location": "38.5,-8.767309999999952"
 },
 {
  "id": 4,
  "name": "Fernando Caetano",
  "company": "Caves da Montanha-A Henriques Lda",
  "email": "fcaetano@cavesdamontanha.pt",
  "phone": "+351 231512260",
  "location": "38.59738,-7.878330000000005"
 },
 {
  "id": 5,
  "name": "André Gonçalves Pires",
  "company": "Caves Vale do Rodo C.R.L ",
  "email": "agp@valedorolo.pt",
  "phone": "+351 254320358",
  "location": "40.05115,-7.363839999999982"
 },
 {
  "id": 6,
  "name": "Rosa Stephens",
  "company": "DOMINGOS ALVES DE SOUSA",
  "email": "rosastephens@alvesdesousa.com",
  "phone": "+351 254822111",
  "location": "40.2293,-8.376330000000053"
 },
 {
  "id": 7,
  "name": "César Alves da Costa",
  "company": "ENCOSTAS DO DOURO",
  "email": "daosul@daosul.com",
  "phone": "+351 232960140",
  "location": "40.74378,-8.694549999999936"
 },
 {
  "id": 8,
  "name": "Rui Jorge Silva",
  "company": "KOPKE",
  "email": "turismo@sogevinus.com",
  "phone": "+351 223746660",
  "location": "40.65704,-8.65059999999994"
 },
 {
  "id": 9,
  "name": "Carolina Meira",
  "company": "NIEPOORT",
  "email": "info@niepoort-vinhos.com",
  "phone": "+351 254855436",
  "location": "41.11775,-7.587260000000015"
 },
 {
  "id": 10,
  "name": "Daniela Andrade",
  "company": "ABSURDO VINUM",
  "email": "info@absurdovinum.com",
  "phone": "+351 926190285",
  "location": "38.52955,-7.314480000000003"
 },
 {
  "id": 11,
  "name": "Catarina Fernandes",
  "company": "CAVES ALTOVISO",
  "email": "info@cavesaltoviso.com",
  "phone": "+351 234743238",
  "location": "38.9552,-7.309849999999983"
 },
 {
  "id": 12,
  "name": "Delfino Penteado",
  "company": "ABREU AMORIM VINHOS DO DOURO SOC. UNIPESSOAL, LDA",
  "email": "abreuamorim@gmail.com",
  "phone": "+351 234743238",
  "location": "40.10666,-8.215799999999945"
 },
 {
  "id": 13,
  "name": "João Pedro Vieira",
  "company": "ADEGA COOPERATIVA DE ALIJÓ, CRL, LDA",
  "email": "dalijob@mail.telepac.pt",
  "phone": "+351 259959101",
  "location": "38.81359,-7.5291200000000345"
 },
 {
  "id": 14,
  "name": "Duarte Neiva Costa",
  "company": "ACÁCIO MORAIS MESQUITA",
  "email": "acaciomorais@gmail.com",
  "phone": "+351 254312976",
  "location": "38.52116,-8.628690000000006"
 },
 {
  "id": 20,
  "name": "John Doe 6 ",
  "company": "CLIENTE 6",
  "email": "cliente6@gmail.com",
  "phone": "+351 254312976",
  "location": "40.69492,-7.604440000000068"
 },
 {
  "id": 21,
  "name": "John Doe 7 ",
  "company": "CLIENTE 7",
  "email": "cliente7@gmail.com",
  "phone": "+351 254312976",
  "location": "38.67975,-8.30637999999999"
 }
];

db.clients.save(clients);
