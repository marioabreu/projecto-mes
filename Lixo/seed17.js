db.dropDatabase();

var clients = [
 {
  "_id": "54087e3fe8e30b7fcd494485",
  "id": 0,
  "name": "Maria Helena Jesus",
  "company": "AVELEDA, SA",
  "email": "info@aveleda.pt",
  "phone": "+351 255718200",
  "location": "38.86072,-8.50399"
 },
 {
  "_id": "54087e3fe8e30b7fcd494486",
  "id": 1,
  "name": "Manuel António",
  "company": "BAGO DE TOURIGA, VINHOS LDA",
  "email": "manuelantonio@bagodetouriga.pt",
  "phone": "+351 932667148",
  "location": "40.23429,-7.22561"
 },
 {
  "_id": "54087e3fe8e30b7fcd494487",
  "id": 2,
  "name": "João Rodrigues da Silva",
  "company": "CARM",
  "email": "jrsilva@aquasseur.com",
  "phone": "+351 279718010",
  "location": "39.70173,-7.49501"
 },
 {
  "_id": "54087e3fe8e30b7fcd494488",
  "id": 3,
  "name": "Joana Santos Ferreirinha",
  "company": "Casa Ferreirinha",
  "email": "jferreirinha@ferreirinha.pt",
  "phone": "+351 227838104",
  "location": "38.70549,-8.59116"
 },
 {
  "_id": "54087e3fe8e30b7fcd494489",
  "id": 4,
  "name": "Fernando Caetano",
  "company": "Caves da Montanha-A Henriques Lda",
  "email": "fcaetano@cavesdamontanha.pt",
  "phone": "+351 231512260",
  "location": "41.64893,-8.13158"
 },
 {
  "_id": "54087e3fe8e30b7fcd49448a",
  "id": 5,
  "name": "André Gonçalves Pires",
  "company": "Caves Vale do Rodo C.R.L ",
  "email": "agp@valedorolo.pt",
  "phone": "+351 254320358",
  "location": "40.39073,-7.23885"
 },
 {
  "_id": "54087e3fe8e30b7fcd49448b",
  "id": 6,
  "name": "Rosa Stephens",
  "company": "DOMINGOS ALVES DE SOUSA",
  "email": "rosastephens@alvesdesousa.com",
  "phone": "+351 254822111",
  "location": "38.41603,-8.04884"
 },
 {
  "_id": "54087e3fe8e30b7fcd49448c",
  "id": 7,
  "name": "César Alves da Costa",
  "company": "ENCOSTAS DO DOURO",
  "email": "daosul@daosul.com",
  "phone": "+351 232960140",
  "location": "41.49508,-7.67425"
 },
 {
  "_id": "54087e3fe8e30b7fcd49448d",
  "id": 8,
  "name": "Rui Jorge Silva",
  "company": "KOPKE",
  "email": "turismo@sogevinus.com",
  "phone": "+351 223746660",
  "location": "39.70868,-8.40705"
 },
 {
  "_id": "54087e3fe8e30b7fcd49448e",
  "id": 9,
  "name": "Carolina Meira",
  "company": "NIEPOORT",
  "email": "info@niepoort-vinhos.com",
  "phone": "+351 254855436",
  "location": "38.18189,-6.91936"
 },
 {
  "_id": "54087e3fe8e30b7fcd49448f",
  "id": 10,
  "name": "Daniela Andrade",
  "company": "ABSURDO VINUM",
  "email": "info@absurdovinum.com",
  "phone": "+351 926190285",
  "location": "39.87778,-7.89034"
 },
 {
  "_id": "54087e3fe8e30b7fcd494490",
  "id": 11,
  "name": "Catarina Fernandes",
  "company": "CAVES ALTOVISO",
  "email": "info@cavesaltoviso.com",
  "phone": "+351 234743238",
  "location": "39.05369,-7.88364"
 },
 {
  "_id": "54087e3fe8e30b7fcd494491",
  "id": 12,
  "name": "Delfino Penteado",
  "company": "ABREU AMORIM VINHOS DO DOURO SOC. UNIPESSOAL, LDA",
  "email": "abreuamorim@gmail.com",
  "phone": "+351 234743238",
  "location": "40.1293,-8.14505"
 },
 {
  "_id": "54087e3fe8e30b7fcd494492",
  "id": 13,
  "name": "João Pedro Vieira",
  "company": "ADEGA COOPERATIVA DE ALIJÓ, CRL, LDA",
  "email": "dalijob@mail.telepac.pt",
  "phone": "+351 259959101",
  "location": "40.63779,-8.46103"
 },
 {
  "_id": "54087e3fe8e30b7fcd494493",
  "id": 14,
  "name": "Duarte Neiva Costa",
  "company": "ACÁCIO MORAIS MESQUITA",
  "email": "acaciomorais@gmail.com",
  "phone": "+351 254312976",
  "location": "40.76994,-8.53765"
 },
 {
  "_id": "54087e3fe8e30b7fcd494494",
  "id": 15,
  "name": "John Doe 2",
  "company": "CLIENTE 2",
  "email": "cliente2@gmail.com",
  "phone": "+351 254312976",
  "location": "40.83737,-8.64443"
 },
 {
  "_id": "54087e3fe8e30b7fcd494495",
  "id": 16,
  "name": "John Doe 3 ",
  "company": "CLIENTE 3",
  "email": "cliente3@gmail.com",
  "phone": "+351 254312976",
  "location": "41.23002,-7.32637"
 },
 {
  "_id": "54087e3fe8e30b7fcd494496",
  "id": 17,
  "name": "John Doe 3",
  "company": "CLIENTE 3",
  "email": "cliente3@gmail.com",
  "phone": "+351 254312976",
  "location": "40.49344,-7.55167"
 },
 {
  "_id": "54087e3fe8e30b7fcd494497",
  "id": 18,
  "name": "John Doe 4",
  "company": "CLIENTE 4",
  "email": "cliente4@gmail.com",
  "phone": "+351 254312976",
  "location": "41.92388,-8.51818"
 },
 {
  "_id": "54087e3fe8e30b7fcd494498",
  "id": 19,
  "name": "John Doe 5 ",
  "company": "CLIENTE 5",
  "email": "cliente5@gmail.com",
  "phone": "+351 254312976",
  "location": "38.96402,-8.33924"
 },
 {
  "_id": "54087e3fe8e30b7fcd494499",
  "id": 20,
  "name": "John Doe 6 ",
  "company": "CLIENTE 6",
  "email": "cliente6@gmail.com",
  "phone": "+351 254312976",
  "location": "41.91959,-8.13811"
 },
 {
  "_id": "54087e3fe8e30b7fcd49449a",
  "id": 21,
  "name": "John Doe 7 ",
  "company": "CLIENTE 7",
  "email": "cliente7@gmail.com",
  "phone": "+351 254312976",
  "location": "40.32866,-6.9923"
 },
 {
  "_id": "54087e3fe8e30b7fcd49449b",
  "id": 22,
  "name": "John Doe 8 ",
  "company": "CLIENTE 8",
  "email": "cliente8@gmail.com",
  "phone": "+351 254312976",
  "location": "38.97017,-7.03794"
 },
 {
  "_id": "54087e3fe8e30b7fcd49449c",
  "id": 23,
  "name": "John Doe 9 ",
  "company": "CLIENTE 9",
  "email": "cliente9@gmail.com",
  "phone": "+351 254312976",
  "location": "38.6855,-8.57446"
 },
 {
  "_id": "54087e3fe8e30b7fcd49449d",
  "id": 24,
  "name": "John Doe 10 ",
  "company": "CLIENTE 10",
  "email": "cliente10@gmail.com",
  "phone": "+351 254312976",
  "location": "41.7775,-8.79434"
 }
];

db.clients.save(clients);
