[
 {
  "_id": "541fe2d1ea35e329bbd0757b",
  "id": 0,
  "name": "Maria Helena Jesus",
  "company": "AVELEDA, SA",
  "email": "info@aveleda.pt",
  "phone": "+351 255718200",
  "location": "40.32421,-7.908239999999978"
 },
 {
  "_id": "541fe2d1ea35e329bbd0757c",
  "id": 1,
  "name": "Manuel António",
  "company": "BAGO DE TOURIGA, VINHOS LDA",
  "email": "manuelantonio@bagodetouriga.pt",
  "phone": "+351 932667148",
  "location": "40.8459,-7.8094200000000455"
 },
 {
  "_id": "541fe2d1ea35e329bbd0757d",
  "id": 2,
  "name": "João Rodrigues da Silva",
  "company": "CARM",
  "email": "jrsilva@aquasseur.com",
  "phone": "+351 279718010",
  "location": "37.48031,-8.633939999999939"
 },
 {
  "_id": "541fe2d1ea35e329bbd0757e",
  "id": 3,
  "name": "Joana Santos Ferreirinha",
  "company": "Casa Ferreirinha",
  "email": "jferreirinha@ferreirinha.pt",
  "phone": "+351 227838104",
  "location": "40.08251,-7.562840000000051"
 },
 {
  "_id": "541fe2d1ea35e329bbd0757f",
  "id": 4,
  "name": "Fernando Caetano",
  "company": "Caves da Montanha-A Henriques Lda",
  "email": "fcaetano@cavesdamontanha.pt",
  "phone": "+351 231512260",
  "location": "38.06685,-7.6642100000000255"
 },
 {
  "_id": "541fe2d1ea35e329bbd07580",
  "id": 5,
  "name": "André Gonçalves Pires",
  "company": "Caves Vale do Rodo C.R.L ",
  "email": "agp@valedorolo.pt",
  "phone": "+351 254320358",
  "location": "41.38761,-7.895089999999982"
 },
 {
  "_id": "541fe2d1ea35e329bbd07581",
  "id": 6,
  "name": "Rosa Stephens",
  "company": "DOMINGOS ALVES DE SOUSA",
  "email": "rosastephens@alvesdesousa.com",
  "phone": "+351 254822111",
  "location": "37.96105,-7.743749999999977"
 },
 {
  "_id": "541fe2d1ea35e329bbd07582",
  "id": 7,
  "name": "César Alves da Costa",
  "company": "ENCOSTAS DO DOURO",
  "email": "daosul@daosul.com",
  "phone": "+351 232960140",
  "location": "38.12531,-8.126960000000054"
 },
 {
  "_id": "541fe2d1ea35e329bbd07583",
  "id": 8,
  "name": "Rui Jorge Silva",
  "company": "KOPKE",
  "email": "turismo@sogevinus.com",
  "phone": "+351 223746660",
  "location": "37.24893,-8.596389999999928"
 },
 {
  "_id": "541fe2d1ea35e329bbd07584",
  "id": 9,
  "name": "Carolina Meira",
  "company": "NIEPOORT",
  "email": "info@niepoort-vinhos.com",
  "phone": "+351 254855436",
  "location": "39.36789,-7.570159999999987"
 },
 {
  "_id": "541fe2d1ea35e329bbd07585",
  "id": 10,
  "name": "Daniela Andrade",
  "company": "ABSURDO VINUM",
  "email": "info@absurdovinum.com",
  "phone": "+351 926190285",
  "location": "39.97203,-7.711000000000013"
 },
 {
  "_id": "541fe2d1ea35e329bbd07586",
  "id": 11,
  "name": "Catarina Fernandes",
  "company": "CAVES ALTOVISO",
  "email": "info@cavesaltoviso.com",
  "phone": "+351 234743238",
  "location": "39.52186,-8.295889999999986"
 },
 {
  "_id": "541fe2d1ea35e329bbd07587",
  "id": 12,
  "name": "Delfino Penteado",
  "company": "ABREU AMORIM VINHOS DO DOURO SOC. UNIPESSOAL, LDA",
  "email": "abreuamorim@gmail.com",
  "phone": "+351 234743238",
  "location": "41.33108,-7.5554200000000264"
 },
 {
  "_id": "541fe2d1ea35e329bbd07588",
  "id": 13,
  "name": "João Pedro Vieira",
  "company": "ADEGA COOPERATIVA DE ALIJÓ, CRL, LDA",
  "email": "dalijob@mail.telepac.pt",
  "phone": "+351 259959101",
  "location": "39.09241,-7.843389999999999"
 },
 {
  "_id": "541fe2d1ea35e329bbd07589",
  "id": 14,
  "name": "Duarte Neiva Costa",
  "company": "ACÁCIO MORAIS MESQUITA",
  "email": "acaciomorais@gmail.com",
  "phone": "+351 254312976",
  "location": "37.53452,-7.550960000000032"
 },
 {
  "_id": "541fe2d1ea35e329bbd0758a",
  "id": 15,
  "name": "John Doe 2",
  "company": "CLIENTE 2",
  "email": "cliente2@gmail.com",
  "phone": "+351 254312976",
  "location": "41.47907,-8.62426000000005"
 },
 {
  "_id": "541fe2d1ea35e329bbd0758b",
  "id": 16,
  "name": "John Doe 3 ",
  "company": "CLIENTE 3",
  "email": "cliente3@gmail.com",
  "phone": "+351 254312976",
  "location": "38.74521,-8.169999999999959"
 },
 {
  "_id": "541fe2d1ea35e329bbd0758c",
  "id": 17,
  "name": "John Doe 3",
  "company": "CLIENTE 3",
  "email": "cliente3@gmail.com",
  "phone": "+351 254312976",
  "location": "37.93215,-7.686339999999973"
 },
 {
  "_id": "541fe2d1ea35e329bbd0758d",
  "id": 18,
  "name": "John Doe 4",
  "company": "CLIENTE 4",
  "email": "cliente4@gmail.com",
  "phone": "+351 254312976",
  "location": "38.56922,-8.107960000000048"
 },
 {
  "_id": "541fe2d1ea35e329bbd0758e",
  "id": 19,
  "name": "John Doe 5 ",
  "company": "CLIENTE 5",
  "email": "cliente5@gmail.com",
  "phone": "+351 254312976",
  "location": "39.98677,-7.686590000000024"
 },
 {
  "_id": "541fe2d1ea35e329bbd0758f",
  "id": 20,
  "name": "John Doe 6 ",
  "company": "CLIENTE 6",
  "email": "cliente6@gmail.com",
  "phone": "+351 254312976",
  "location": "38.25474,-8.019929999999931"
 },
 {
  "_id": "541fe2d1ea35e329bbd07590",
  "id": 21,
  "name": "John Doe 7 ",
  "company": "CLIENTE 7",
  "email": "cliente7@gmail.com",
  "phone": "+351 254312976",
  "location": "39.29438,-7.482259999999997"
 },
 {
  "_id": "541fe2d1ea35e329bbd07591",
  "id": 22,
  "name": "John Doe 8 ",
  "company": "CLIENTE 8",
  "email": "cliente8@gmail.com",
  "phone": "+351 254312976",
  "location": "41.6295,-8.33483000000001"
 },
 {
  "_id": "541fe2d1ea35e329bbd07592",
  "id": 23,
  "name": "John Doe 9 ",
  "company": "CLIENTE 9",
  "email": "cliente9@gmail.com",
  "phone": "+351 254312976",
  "location": "37.7531,-8.10376999999994"
 },
 {
  "_id": "541fe2d1ea35e329bbd07593",
  "id": 24,
  "name": "John Doe 10 ",
  "company": "CLIENTE 10",
  "email": "cliente10@gmail.com",
  "phone": "+351 254312976",
  "location": "37.6702,-8.061270000000036"
 }
]