db.dropDatabase();

var clients = [
 {
  "id": 0,
  "name": "Maria Helena Jesus",
  "company": "AVELEDA, SA",
  "email": "info@aveleda.pt",
  "phone": "+351 255718200",
  "location": "40.88004,-8.57068"
},
 {
  "id": 1,
  "name": "Manuel António",
  "company": "BAGO DE TOURIGA, VINHOS LDA",
  "email": "manuelantonio@bagodetouriga.pt",
  "phone": "+351 932667148",
  "location": "38.81759,-6.98979"
 },
 {
  "id": 2,
  "name": "João Rodrigues da Silva",
  "company": "CARM",
  "email": "jrsilva@aquasseur.com",
  "phone": "+351 279718010",
  "location": "38.85088,-8.71994"
 },
 {
  "id": 3,
  "name": "Joana Santos Ferreirinha",
  "company": "Casa Ferreirinha",
  "email": "jferreirinha@ferreirinha.pt",
  "phone": "+351 227838104",
  "location": "41.94607,-8.5732"
 },
 {
  "id": 4,
  "name": "Fernando Caetano",
  "company": "Caves da Montanha-A Henriques Lda",
  "email": "fcaetano@cavesdamontanha.pt",
  "phone": "+351 231512260",
  "location": "40.00833,-8.81264"
 },
 {
  "id": 5,
  "name": "André Gonçalves Pires",
  "company": "Caves Vale do Rodo C.R.L ",
  "email": "agp@valedorolo.pt",
  "phone": "+351 254320358",
  "location": "39.54518,-8.32551"
 },
 {
  "id": 6,
  "name": "Rosa Stephens",
  "company": "DOMINGOS ALVES DE SOUSA",
  "email": "rosastephens@alvesdesousa.com",
  "phone": "+351 254822111",
  "location": "39.9,-7.30865"
 },
 {
  "id": 7,
  "name": "César Alves da Costa",
  "company": "ENCOSTAS DO DOURO",
  "email": "daosul@daosul.com",
  "phone": "+351 232960140",
  "location": "39.92887,-8.24236"
 },
 {
  "id": 8,
  "name": "Rui Jorge Silva",
  "company": "KOPKE",
  "email": "turismo@sogevinus.com",
  "phone": "+351 223746660",
  "location": "39.57668,-8.71976"
 },
 {
  "id": 9,
  "name": "Carolina Meira",
  "company": "NIEPOORT",
  "email": "info@niepoort-vinhos.com",
  "phone": "+351 254855436",
  "location": "40.48928,-7.46314"
 },
 {
  "id": 10,
  "name": "Daniela Andrade",
  "company": "ABSURDO VINUM",
  "email": "info@absurdovinum.com",
  "phone": "+351 926190285",
  "location": "41.84487,-7.85674"
 },
 { 
  "id": 11,
  "name": "Catarina Fernandes",
  "company": "CAVES ALTOVISO",
  "email": "info@cavesaltoviso.com",
  "phone": "+351 234743238",
  "location": "41.70642,-7.85405"
},
 { 
  "id": 12,
  "name": "Delfino Penteado",
  "company": "ABREU AMORIM VINHOS DO DOURO SOC. UNIPESSOAL, LDA",
  "email": "abreuamorim@gmail.com",
  "phone": "+351 234743238",
  "location": "38.48383,-8.43501"
 },
 {
  "id": 13,
  "name": "João Pedro Vieira",
  "company": "ADEGA COOPERATIVA DE ALIJÓ, CRL, LDA",
  "email": "dalijob@mail.telepac.pt",
  "phone": "+351 259959101",
  "location": "40.82839,-7.31006"
 },
 {  
  "id": 14,
  "name": "Duarte Neiva Costa",
  "company": "ACÁCIO MORAIS MESQUITA",
  "email": "acaciomorais@gmail.com",
  "phone": "+351 254312976",
  "location": "40.72272,-8.63543"
 }
];

db.clients.save(clients);
