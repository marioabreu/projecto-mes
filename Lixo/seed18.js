db.dropDatabase();

var clients = [
 {
  "id": 0,
  "name": "Maria Helena Jesus",
  "company": "AVELEDA, SA",
  "email": "info@aveleda.pt",
  "phone": "+351 255718200",
  "location": "40.51473,-7.1012699999999995"
 },
 {
  "id": 1,
  "name": "Manuel António",
  "company": "BAGO DE TOURIGA, VINHOS LDA",
  "email": "manuelantonio@bagodetouriga.pt",
  "phone": "+351 932667148",
  "location": "38.04691,-7.262809999999945"
 },
 {
  "id": 2,
  "name": "João Rodrigues da Silva",
  "company": "CARM",
  "email": "jrsilva@aquasseur.com",
  "phone": "+351 279718010",
  "location": "39.24676,-7.636629999999968"
 },
 {
  "id": 3,
  "name": "Joana Santos Ferreirinha",
  "company": "Casa Ferreirinha",
  "email": "jferreirinha@ferreirinha.pt",
  "phone": "+351 227838104",
  "location": "40.09943,-7.139189999999985"
 },
 {
  "id": 4,
  "name": "Fernando Caetano",
  "company": "Caves da Montanha-A Henriques Lda",
  "email": "fcaetano@cavesdamontanha.pt",
  "phone": "+351 231512260",
  "location": "39.88142,-8.57800999999995"
 },
 {
  "id": 5,
  "name": "André Gonçalves Pires",
  "company": "Caves Vale do Rodo C.R.L ",
  "email": "agp@valedorolo.pt",
  "phone": "+351 254320358",
  "location": "41.38423,-8.030560000000037"
 },
 {
  "id": 6,
  "name": "Rosa Stephens",
  "company": "DOMINGOS ALVES DE SOUSA",
  "email": "rosastephens@alvesdesousa.com",
  "phone": "+351 254822111",
  "location": "39.22016,-7.360379999999964"
 },
 {
  "id": 7,
  "name": "César Alves da Costa",
  "company": "ENCOSTAS DO DOURO",
  "email": "daosul@daosul.com",
  "phone": "+351 232960140",
  "location": "39.87682,-7.27012000000002"
 },
 {
  "id": 8,
  "name": "Rui Jorge Silva",
  "company": "KOPKE",
  "email": "turismo@sogevinus.com",
  "phone": "+351 223746660",
  "location": "39.99947,-7.135220000000004"
 },
 {
  "id": 9,
  "name": "Carolina Meira",
  "company": "NIEPOORT",
  "email": "info@niepoort-vinhos.com",
  "phone": "+351 254855436",
  "location": "41.70933,-7.253590000000031"
 },
 {
  "id": 10,
  "name": "Daniela Andrade",
  "company": "ABSURDO VINUM",
  "email": "info@absurdovinum.com",
  "phone": "+351 926190285",
  "location": "38.71312,-8.64864"
 },
 {
  "id": 11,
  "name": "Catarina Fernandes",
  "company": "CAVES ALTOVISO",
  "email": "info@cavesaltoviso.com",
  "phone": "+351 234743238",
  "location": "41.16897,-7.2207200000000284"
 },
 {
  "id": 12,
  "name": "Delfino Penteado",
  "company": "ABREU AMORIM VINHOS DO DOURO SOC. UNIPESSOAL, LDA",
  "email": "abreuamorim@gmail.com",
  "phone": "+351 234743238",
  "location": "41.01867,-7.21717000000001"
 },
 {
  "id": 13,
  "name": "João Pedro Vieira",
  "company": "ADEGA COOPERATIVA DE ALIJÓ, CRL, LDA",
  "email": "dalijob@mail.telepac.pt",
  "phone": "+351 259959101",
  "location": "38.0422,-7.066559999999981"
 },
 {
  "id": 14,
  "name": "Duarte Neiva Costa",
  "company": "ACÁCIO MORAIS MESQUITA",
  "email": "acaciomorais@gmail.com",
  "phone": "+351 254312976",
  "location": "39.63492,-7.867690000000039"
 },
 {
  "id": 15,
  "name": "John Doe 2",
  "company": "CLIENTE 2",
  "email": "cliente2@gmail.com",
  "phone": "+351 254312976",
  "location": "40.09806,-7.32335999999998"
 },
 {
  "id": 16,
  "name": "John Doe 3 ",
  "company": "CLIENTE 3",
  "email": "cliente3@gmail.com",
  "phone": "+351 254312976",
  "location": "39.48711,-7.1637699999999995"
 },
 {
  "id": 17,
  "name": "John Doe 3",
  "company": "CLIENTE 3",
  "email": "cliente3@gmail.com",
  "phone": "+351 254312976",
  "location": "38.55181,-8.473099999999931"
 },
 {
  "id": 18,
  "name": "John Doe 4",
  "company": "CLIENTE 4",
  "email": "cliente4@gmail.com",
  "phone": "+351 254312976",
  "location": "41.72661,-8.594460000000026"
 },
 {
  "id": 19,
  "name": "John Doe 5 ",
  "company": "CLIENTE 5",
  "email": "cliente5@gmail.com",
  "phone": "+351 254312976",
  "location": "41.00908,-7.459730000000036"
 },
 {
  "id": 20,
  "name": "John Doe 6 ",
  "company": "CLIENTE 6",
  "email": "cliente6@gmail.com",
  "phone": "+351 254312976",
  "location": "40.00292,-7.186590000000024"
 },
 {
  "id": 21,
  "name": "John Doe 7 ",
  "company": "CLIENTE 7",
  "email": "cliente7@gmail.com",
  "phone": "+351 254312976",
  "location": "39.10816,-7.138580000000047"
 },
 {
  "id": 22,
  "name": "John Doe 8 ",
  "company": "CLIENTE 8",
  "email": "cliente8@gmail.com",
  "phone": "+351 254312976",
  "location": "41.31068,-8.619760000000042"
 },
 {
  "id": 23,
  "name": "John Doe 9 ",
  "company": "CLIENTE 9",
  "email": "cliente9@gmail.com",
  "phone": "+351 254312976",
  "location": "38.37592,-7.551249999999982"
 },
 {
  "id": 24,
  "name": "John Doe 10 ",
  "company": "CLIENTE 10",
  "email": "cliente10@gmail.com",
  "phone": "+351 254312976",
  "location": "39.85228,-7.205349999999953"
 }
];

db.clients.save(clients);
