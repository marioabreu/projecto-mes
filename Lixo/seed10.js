db.dropDatabase();

var clients = [
 {
  "id": 0,
  "name": "Maria Helena Jesus",
  "company": "AVELEDA, SA",
  "email": "info@aveleda.pt",
  "phone": "+351 255718200",
  "location": "39.99679,-8.050690000000031"
 },
 {
  "id": 1,
  "name": "Manuel António",
  "company": "BAGO DE TOURIGA, VINHOS LDA",
  "email": "manuelantonio@bagodetouriga.pt",
  "phone": "+351 932667148",
  "location": "40.26902,-8.786600000000021"
 },
 {
  "id": 2,
  "name": "João Rodrigues da Silva",
  "company": "CARM",
  "email": "jrsilva@aquasseur.com",
  "phone": "+351 279718010",
  "location": "38.93528,-6.947689999999966"
 },
 {
  "id": 3,
  "name": "Joana Santos Ferreirinha",
  "company": "Casa Ferreirinha",
  "email": "jferreirinha@ferreirinha.pt",
  "phone": "+351 227838104",
  "location": "39.19909,-7.805650000000014"
 },
 {
  "id": 4,
  "name": "Fernando Caetano",
  "company": "Caves da Montanha-A Henriques Lda",
  "email": "fcaetano@cavesdamontanha.pt",
  "phone": "+351 231512260",
  "location": "41.21821,-8.208129999999983"
 },
 {
  "id": 5,
  "name": "André Gonçalves Pires",
  "company": "Caves Vale do Rodo C.R.L ",
  "email": "agp@valedorolo.pt",
  "phone": "+351 254320358",
  "location": "39.30668,-7.820259999999962"
 },
 {
  "id": 6,
  "name": "Rosa Stephens",
  "company": "DOMINGOS ALVES DE SOUSA",
  "email": "rosastephens@alvesdesousa.com",
  "phone": "+351 254822111",
  "location": "39.55464,-7.29507000000001"
 },
 {
  "id": 7,
  "name": "César Alves da Costa",
  "company": "ENCOSTAS DO DOURO",
  "email": "daosul@daosul.com",
  "phone": "+351 232960140",
  "location": "40.85929,-7.258050000000026"
 },
 {
  "id": 8,
  "name": "Rui Jorge Silva",
  "company": "KOPKE",
  "email": "turismo@sogevinus.com",
  "phone": "+351 223746660",
  "location": "41.23918,-8.59292000000005"
 },
 {
  "id": 9,
  "name": "Carolina Meira",
  "company": "NIEPOORT",
  "email": "info@niepoort-vinhos.com",
  "phone": "+351 254855436",
  "location": "41.42271,-7.114100000000008"
 },
 {
  "id": 10,
  "name": "Daniela Andrade",
  "company": "ABSURDO VINUM",
  "email": "info@absurdovinum.com",
  "phone": "+351 926190285",
  "location": "41.58111,-7.7187300000000505"
 },
 {
  "id": 11,
  "name": "Catarina Fernandes",
  "company": "CAVES ALTOVISO",
  "email": "info@cavesaltoviso.com",
  "phone": "+351 234743238",
  "location": "39.43504,-8.626030000000014"
 },
 {
  "id": 12,
  "name": "Delfino Penteado",
  "company": "ABREU AMORIM VINHOS DO DOURO SOC. UNIPESSOAL, LDA",
  "email": "abreuamorim@gmail.com",
  "phone": "+351 234743238",
  "location": "39.26943,-7.066029999999955"
 },
 {
  "id": 13,
  "name": "João Pedro Vieira",
  "company": "ADEGA COOPERATIVA DE ALIJÓ, CRL, LDA",
  "email": "dalijob@mail.telepac.pt",
  "phone": "+351 259959101",
  "location": "38.88602,-7.105800000000045"
 },
 {
  "id": 14,
  "name": "Duarte Neiva Costa",
  "company": "ACÁCIO MORAIS MESQUITA",
  "email": "acaciomorais@gmail.com",
  "phone": "+351 254312976",
  "location": "39.27084,-8.672329999999988"
 },
 {
  "id": 17,
  "name": "John Doe 3",
  "company": "CLIENTE 3",
  "email": "cliente3@gmail.com",
  "phone": "+351 254312976",
  "location": "41.57075,-6.90014999999994"
 },
 {
  "id": 20,
  "name": "John Doe 6 ",
  "company": "CLIENTE 6",
  "email": "cliente6@gmail.com",
  "phone": "+351 254312976",
  "location": "41.17915,-7.919179999999983"
 },
 {
  "id": 21,
  "name": "John Doe 7 ",
  "company": "CLIENTE 7",
  "email": "cliente7@gmail.com",
  "phone": "+351 254312976",
  "location": "38.02242,-7.245090000000005"
 },
 {
  "id": 22,
  "name": "John Doe 8 ",
  "company": "CLIENTE 8",
  "email": "cliente8@gmail.com",
  "phone": "+351 254312976",
  "location": "38.15038,-7.562049999999999"
 },
 {
  "id": 24,
  "name": "John Doe 10 ",
  "company": "CLIENTE 10",
  "email": "cliente10@gmail.com",
  "phone": "+351 254312976",
  "location": "39.87287,-8.458700000000022"
 }
];

db.clients.save(clients);
