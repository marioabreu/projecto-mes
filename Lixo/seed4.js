db.dropDatabase();

var clients = [
 {
  "id": 0,
  "name": "Maria Helena Jesus",
  "company": "AVELEDA, SA",
  "email": "info@aveleda.pt",
  "phone": "+351 255718200",
  "location": "39.1755,-7.471599999999967"
 },
 {
  "id": 1,
  "name": "Manuel António",
  "company": "BAGO DE TOURIGA, VINHOS LDA",
  "email": "manuelantonio@bagodetouriga.pt",
  "phone": "+351 932667148",
  "location": "40.63953,-7.486130000000003"
 },
 {
  "id": 2,
  "name": "João Rodrigues da Silva",
  "company": "CARM",
  "email": "jrsilva@aquasseur.com",
  "phone": "+351 279718010",
  "location": "38.1596,-8.221220000000017"
 },
 {
  "id": 3,
  "name": "Joana Santos Ferreirinha",
  "company": "Casa Ferreirinha",
  "email": "jferreirinha@ferreirinha.pt",
  "phone": "+351 227838104",
  "location": "40.38808,-8.40206999999998"
 },
 {
  "id": 4,
  "name": "Fernando Caetano",
  "company": "Caves da Montanha-A Henriques Lda",
  "email": "fcaetano@cavesdamontanha.pt",
  "phone": "+351 231512260",
  "location": "38.94057,-6.997060000000033"
 },
 {
  "id": 5,
  "name": "André Gonçalves Pires",
  "company": "Caves Vale do Rodo C.R.L ",
  "email": "agp@valedorolo.pt",
  "phone": "+351 254320358",
  "location": "39.1096,-8.894499999999994"
 },
 {
  "id": 6,
  "name": "Rosa Stephens",
  "company": "DOMINGOS ALVES DE SOUSA",
  "email": "rosastephens@alvesdesousa.com",
  "phone": "+351 254822111",
  "location": "39.12705,-7.0491799999999785"
 },
 {
  "id": 7,
  "name": "César Alves da Costa",
  "company": "ENCOSTAS DO DOURO",
  "email": "daosul@daosul.com",
  "phone": "+351 232960140",
  "location": "38.34876,-7.401039999999966"
 },
 {
  "id": 8,
  "name": "Rui Jorge Silva",
  "company": "KOPKE",
  "email": "turismo@sogevinus.com",
  "phone": "+351 223746660",
  "location": "39.92917,-6.920510000000036"
 },
 {
  "id": 9,
  "name": "Carolina Meira",
  "company": "NIEPOORT",
  "email": "info@niepoort-vinhos.com",
  "phone": "+351 254855436",
  "location": "39.71112,-8.549909999999954"
 },
 {
  "id": 10,
  "name": "Daniela Andrade",
  "company": "ABSURDO VINUM",
  "email": "info@absurdovinum.com",
  "phone": "+351 926190285",
  "location": "41.38408,-6.901960000000031"
 },
 {
  "id": 11,
  "name": "Catarina Fernandes",
  "company": "CAVES ALTOVISO",
  "email": "info@cavesaltoviso.com",
  "phone": "+351 234743238",
  "location": "40.09191,-7.967489999999998"
 },
 {
  "id": 12,
  "name": "Delfino Penteado",
  "company": "ABREU AMORIM VINHOS DO DOURO SOC. UNIPESSOAL, LDA",
  "email": "abreuamorim@gmail.com",
  "phone": "+351 234743238",
  "location": "40.42045,-8.234320000000025"
 },
 {
  "id": 13,
  "name": "João Pedro Vieira",
  "company": "ADEGA COOPERATIVA DE ALIJÓ, CRL, LDA",
  "email": "dalijob@mail.telepac.pt",
  "phone": "+351 259959101",
  "location": "41.87323,-7.506470000000036"
 },
 {
  "id": 14,
  "name": "Duarte Neiva Costa",
  "company": "ACÁCIO MORAIS MESQUITA",
  "email": "acaciomorais@gmail.com",
  "phone": "+351 254312976",
  "location": "41.15057,-7.788149999999973"
 },
 {
  "id": 15,
  "name": "John Doe 2",
  "company": "CLIENTE 2",
  "email": "cliente2@gmail.com",
  "phone": "+351 254312976",
  "location": "39.8535,-7.392349999999965"
 },
 {
  "id": 16,
  "name": "John Doe 3 ",
  "company": "CLIENTE 3",
  "email": "cliente3@gmail.com",
  "phone": "+351 254312976",
  "location": "40.67383,-7.726650000000063"
 },
 {
  "id": 17,
  "name": "John Doe 3",
  "company": "CLIENTE 3",
  "email": "cliente3@gmail.com",
  "phone": "+351 254312976",
  "location": "38.64284,-8.223960000000034"
 },
 {
  "id": 18,
  "name": "John Doe 4",
  "company": "CLIENTE 4",
  "email": "cliente4@gmail.com",
  "phone": "+351 254312976",
  "location": "38.30056,-7.590580000000045"
 },
 {
  "id": 19,
  "name": "John Doe 5 ",
  "company": "CLIENTE 5",
  "email": "cliente5@gmail.com",
  "phone": "+351 254312976",
  "location": "38.12903,-8.728540000000066"
 },
 {
  "id": 20,
  "name": "John Doe 6 ",
  "company": "CLIENTE 6",
  "email": "cliente6@gmail.com",
  "phone": "+351 254312976",
  "location": "38.03574,-7.316509999999994"
 },
 {
  "id": 21,
  "name": "John Doe 7 ",
  "company": "CLIENTE 7",
  "email": "cliente7@gmail.com",
  "phone": "+351 254312976",
  "location": "38.24508,-6.904199999999946"
 },
 {
  "id": 22,
  "name": "John Doe 8 ",
  "company": "CLIENTE 8",
  "email": "cliente8@gmail.com",
  "phone": "+351 254312976",
  "location": "41.91198,-7.942409999999995"
 },
 {
  "id": 23,
  "name": "John Doe 9 ",
  "company": "CLIENTE 9",
  "email": "cliente9@gmail.com",
  "phone": "+351 254312976",
  "location": "39.62049,-8.748010000000022"
 },
 {
  "id": 24,
  "name": "John Doe 10 ",
  "company": "CLIENTE 10",
  "email": "cliente10@gmail.com",
  "phone": "+351 254312976",
  "location": "39.56517,-8.32641000000001"
 }
];

db.clients.save(clients);
