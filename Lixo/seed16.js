db.dropDatabase();

var clients = [
 {
  "id": 0,
  "name": "Maria Helena Jesus",
  "company": "AVELEDA, SA",
  "email": "info@aveleda.pt",
  "phone": "+351 255718200",
  "location": "39.73471,-8.231019999999944"
 },
 {
  "id": 1,
  "name": "Manuel António",
  "company": "BAGO DE TOURIGA, VINHOS LDA",
  "email": "manuelantonio@bagodetouriga.pt",
  "phone": "+351 932667148",
  "location": "38.07596,-8.22293000000002"
 },
 {
  "id": 2,
  "name": "João Rodrigues da Silva",
  "company": "CARM",
  "email": "jrsilva@aquasseur.com",
  "phone": "+351 279718010",
  "location": "40.54493,-7.976980000000026"
 },
 {
  "id": 3,
  "name": "Joana Santos Ferreirinha",
  "company": "Casa Ferreirinha",
  "email": "jferreirinha@ferreirinha.pt",
  "phone": "+351 227838104",
  "location": "39.73298,-8.016290000000026"
 },
 {
  "id": 4,
  "name": "Fernando Caetano",
  "company": "Caves da Montanha-A Henriques Lda",
  "email": "fcaetano@cavesdamontanha.pt",
  "phone": "+351 231512260",
  "location": "41.26068,-7.750130000000013"
 },
 {
  "id": 5,
  "name": "André Gonçalves Pires",
  "company": "Caves Vale do Rodo C.R.L ",
  "email": "agp@valedorolo.pt",
  "phone": "+351 254320358",
  "location": "38.52396,-7.562130000000025"
 },
 {
  "id": 6,
  "name": "Rosa Stephens",
  "company": "DOMINGOS ALVES DE SOUSA",
  "email": "rosastephens@alvesdesousa.com",
  "phone": "+351 254822111",
  "location": "41.83195,-7.565270000000055"
 },
 {
  "id": 7,
  "name": "César Alves da Costa",
  "company": "ENCOSTAS DO DOURO",
  "email": "daosul@daosul.com",
  "phone": "+351 232960140",
  "location": "41.6779,-7.694079999999985"
 },
 {
  "id": 8,
  "name": "Rui Jorge Silva",
  "company": "KOPKE",
  "email": "turismo@sogevinus.com",
  "phone": "+351 223746660",
  "location": "41.89056,-8.021269999999959"
 },
 {
  "id": 9,
  "name": "Carolina Meira",
  "company": "NIEPOORT",
  "email": "info@niepoort-vinhos.com",
  "phone": "+351 254855436",
  "location": "39.56077,-7.061090000000036"
 },
 {
  "id": 10,
  "name": "Daniela Andrade",
  "company": "ABSURDO VINUM",
  "email": "info@absurdovinum.com",
  "phone": "+351 926190285",
  "location": "40.0772,-7.703210000000013"
 },
 {
  "id": 11,
  "name": "Catarina Fernandes",
  "company": "CAVES ALTOVISO",
  "email": "info@cavesaltoviso.com",
  "phone": "+351 234743238",
  "location": "40.18077,-8.463750000000005"
 },
 {
  "id": 12,
  "name": "Delfino Penteado",
  "company": "ABREU AMORIM VINHOS DO DOURO SOC. UNIPESSOAL, LDA",
  "email": "abreuamorim@gmail.com",
  "phone": "+351 234743238",
  "location": "40.51191,-8.675690000000031"
 },
 {
  "id": 13,
  "name": "João Pedro Vieira",
  "company": "ADEGA COOPERATIVA DE ALIJÓ, CRL, LDA",
  "email": "dalijob@mail.telepac.pt",
  "phone": "+351 259959101",
  "location": "41.03399,-7.923430000000053"
 },
 {
  "id": 14,
  "name": "Duarte Neiva Costa",
  "company": "ACÁCIO MORAIS MESQUITA",
  "email": "acaciomorais@gmail.com",
  "phone": "+351 254312976",
  "location": "38.18394,-7.878550000000018"
 },
 {
  "id": 15,
  "name": "John Doe 2",
  "company": "CLIENTE 2",
  "email": "cliente2@gmail.com",
  "phone": "+351 254312976",
  "location": "38.05638,-7.188160000000039"
 },
 {
  "id": 16,
  "name": "John Doe 3 ",
  "company": "CLIENTE 3",
  "email": "cliente3@gmail.com",
  "phone": "+351 254312976",
  "location": "41.24527,-7.329529999999977"
 },
 {
  "id": 17,
  "name": "John Doe 3",
  "company": "CLIENTE 3",
  "email": "cliente3@gmail.com",
  "phone": "+351 254312976",
  "location": "39.83335,-8.666960000000017"
 },
 {
  "id": 18,
  "name": "John Doe 4",
  "company": "CLIENTE 4",
  "email": "cliente4@gmail.com",
  "phone": "+351 254312976",
  "location": "40.28167,-7.317850000000021"
 },
 {
  "id": 19,
  "name": "John Doe 5 ",
  "company": "CLIENTE 5",
  "email": "cliente5@gmail.com",
  "phone": "+351 254312976",
  "location": "40.14908,-7.045190000000048"
 },
 {
  "id": 20,
  "name": "John Doe 6 ",
  "company": "CLIENTE 6",
  "email": "cliente6@gmail.com",
  "phone": "+351 254312976",
  "location": "38.80949,-8.885850000000005"
 },
 {
  "id": 21,
  "name": "John Doe 7 ",
  "company": "CLIENTE 7",
  "email": "cliente7@gmail.com",
  "phone": "+351 254312976",
  "location": "39.37633,-8.620900000000006"
 },
 {
  "id": 22,
  "name": "John Doe 8 ",
  "company": "CLIENTE 8",
  "email": "cliente8@gmail.com",
  "phone": "+351 254312976",
  "location": "38.92361,-7.038080000000036"
 },
 {
  "id": 23,
  "name": "John Doe 9 ",
  "company": "CLIENTE 9",
  "email": "cliente9@gmail.com",
  "phone": "+351 254312976",
  "location": "38.89912,-8.302149999999983"
 },
 {
  "id": 24,
  "name": "John Doe 10 ",
  "company": "CLIENTE 10",
  "email": "cliente10@gmail.com",
  "phone": "+351 254312976",
  "location": "40.01244,-7.639369999999985"
 }
];

db.clients.save(clients);
