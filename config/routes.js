
module.exports = function(router){

  var PagesController = require('../controllers/pages.js');
  var ClientsController = require('../controllers/clients.js')();
  
  console.log('Config Routes');
  router.get('/', PagesController.index);
  router.get('/about', PagesController.about);
  router.get('/contact', PagesController.contact);
  router.get('/routes', PagesController.routes);
  router.get('/statistics', PagesController.statistics);


  router.get('/clients:format?', ClientsController.index);
  router.get('/clients/:id', ClientsController.show);
  router.post('/clients', ClientsController.create);
  router.put('/clients/:id', ClientsController.update);
  router.delete('/clients/:id', ClientsController.delete);
  router.get('/clients/:id/requests', ClientsController.requests);


  // router.get('/requests/:id', ClientsController.show);
  // router.post('/clients/:id/requests/', ClientsController.create);

  return router;
}
