var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// Database
var mongo = require('mongoskin');
var db = mongo.db('mongodb://localhost:27017/dbproject_mes', { safe: true, native_parser: true});

var router = express.Router();

// Initial routes
// Handle routes in file ./config/routes passing router as argument

var routes = require('./config/routes')(router);

// using expressjs
var app = express();
// console.log(routes.red);

//config applicatoin
// view engine setup
app.set('view engine', 'jade');
app.set('views', path.join(__dirname, 'views'));


// use middleware
app.use(favicon());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use(logger('dev'));
// app

// make app acessible by http
app.use(function(req, res, next){
  req.db = db;
  next();
});


// handler for routes
 app.use('/', routes);



/// catch 404 and forward to error handler
app.use(function(err, req, res, next) {
    
    res.status(err.status || 500);
    res.render('error',{
        message: err.message,
        error: err
    });
    next(err);
});
/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
