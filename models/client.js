var mongo = require('mongoskin');
var db = mongo.db('mongodb://localhost:27017/dbproject_mes', { native_parser: true});
var BSON = mongo.BSONPure;

module.exports = function(){
  
  var collection = db.collection('clients');

  var all = function(cb){
    console.log(collection);
    console.log('all');
    return collection.find().toArray(cb);
  };

  var create = function(data, cb){
    console.log('model create');
    
    return collection.insert(data, {w:1}, function(err, records){
      console.log('Record inserted with success!');
      cb(collection.find());
      
    });
  };

  var destroy = function(id, cb){

    console.log('model destroy');
    return collection.removeById(id, function(error, count){
      if (error) return cb(error);
      if (count !== 1) return cb(new Error('Something went wrong!'));
      console.log(cb(count));
    });
  };



//   db.collection('clientlist').removeById(clientToDelete, function(err, result){
//     res.send((result===1) ? {msg: ''} : {msg:'Erro: ' + err});
//   });

  var show = function(id, cb){
    console.log(id);
    var o_id = new BSON.ObjectID(id);
    return collection.find(o_id);

  };

  var requests = function(id, cb){
    
    var client = collection.find(id);
    console.log(client);
    return cb(client);
  }

  return {
    all: all,
    delete: destroy,
    show: show, 
    create: create,
    requests: requests
    // find: find,
    // update: update
  }

}